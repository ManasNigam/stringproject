let result = (givenString) => {
    let answer = ""
    for (let i = 0; i < givenString.length; i++) {
        let character = Number(givenString[i])
        if (
            Number.isFinite(character) ||
            givenString[i] === '.' ||
            givenString[i] === '-'
        ) {
            answer += givenString[i]

        }
    }
    if (answer === "" || isNaN(Number(answer))) {
        return 0
    }

    return Number(answer)

}
module.exports = result

