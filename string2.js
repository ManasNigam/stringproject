const result = (givenString) => {

    let resultArray = givenString.split(".")

    for (let i = 0; i < resultArray.length; i++) {
        resultArray[i] = Number(resultArray[i])
        if (resultArray.length <= 4) {
            if (resultArray[i] > 255 || isNaN(resultArray[i])) {
                return []
            }
        }
    }
    return resultArray

}

module.exports = result